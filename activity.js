// onsale

  db.fruits.aggregate([
        {$match: {onSale:true}},
        {$count: "totalOnSale"},
        {$project: {_id:0}}
      ]);

// onsale >=20

  db.fruits.aggregate([
        {$match: {stock:{$gte:20}}},
        {$count: "enoughStock"},
        {$project: {_id:0}}
      ]);

// average

  db.fruits.aggregate([
        {$match:  {onSale:true}},
        {$group: {_id: "$supplier_id", avg_price: {$avg:"$stock"}}}
    ]);

// max

  db.fruits.aggregate([
        {$match:  {onSale:true}},
        {$group: {_id: "$supplier_id", max_price: {$max:"$stock"}}}
    ]);

// min

  db.fruits.aggregate([
        {$match:  {onSale:true}},
        {$group: {_id: "$supplier_id", min_price: {$min:"$stock"}}}
    ]);

